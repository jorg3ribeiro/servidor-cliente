package cliente;
import java.io.*;  
import java.net.*;
import java.util.Scanner;  

public class clienteTCP {
	public static void main(String[] args) throws UnknownHostException, IOException {
		Socket cliente = new Socket("0.tcp.ngrok.io", 18117);
		System.out.println("Cliente Conectado");

		Scanner sc = new Scanner(System.in);
		PrintStream saida = new PrintStream(cliente.getOutputStream());

		while (sc.hasNext()) {
			saida.println(sc.nextLine());
		}
	}

}
