package servidor;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class servidorTCP {
	

	public static void main(String[] args) throws IOException{  
			
		ServerSocket servidor = new ServerSocket(3000);
		System.out.println("Servidor ouvindo a porta " + 3000);

		while (true) {
			Socket cliente = servidor.accept();
			System.out.println("Cliente conectado: " + cliente.getInetAddress().getHostAddress());

			PrintWriter out = new PrintWriter(cliente.getOutputStream(), true);
		    out.flush();
		    out.println("Mensagem Servidor." + "\r\n");
		    
		    Scanner s = new Scanner(cliente.getInputStream());
			while (s.hasNextLine()) {
				System.out.println(s.nextLine());
			}
		}
	} 
}
